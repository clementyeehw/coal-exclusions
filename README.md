# Overview

The python scripts facilitate the extraction of global companies' coal-related involvements and activities.

---

## Description of Python Scripts

1. FM.py : Extraction of companies' coal involvements from listed funds and asset managers. Currently, the financemap.org site is under beta testing.
2. GEM.py : Extraction of companies' coal involvements from Global Energy Monitor (GEM).
3. IA.py : Extraction of companies' coal involvements from Industry About (IA).

---