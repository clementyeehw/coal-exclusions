# Import Python libraries #
from bs4 import BeautifulSoup
from concurrent.futures import ThreadPoolExecutor
from functools import partial
import datetime as dt
import gc
import numpy as np
import os
import pandas as pd
import re
import requests


######################################################################################################################################
''' 
##########                  ########## 
            WEBSCRAPPING 
##########                  ##########  
'''

def chunks(lst, n):
    '''
    Generate successive n-sized chunks from list.
    Parameters:
        lst           : List containing the elements to be broken into chunks
        n             : Size of each chunk
    Return:
        chunked_list  : List containing n-sized chunks
    '''
    chunked_list = [lst[i:i + n] for i in range(0, len(lst), n)]
    
    return chunked_list


def clean_responses(response, soup=None):
    '''
    Clear all the the responses to free up memory. 
    Parameters:
        response      : Response from GET requests
        soup          : Parsed BeautifulSoup     
    '''
    # Close response #
    response.close()
    response = None
    
    # Decompose soup if soup is valid #
    if soup is not None:
        soup.decompose()
        
    # Clear all the waste #
    gc.collect()
    
    
def get_IA_landings():
    ''' 
    Retrieve the main landing page for all themral power plants in Industry About (IA).
    Return:
        power_df : DataFrame containing local weblinks of thermal power plants
    '''
    # Initiate parameters #
    ## Root IA url ##
    root_url = 'https://www.industryabout.com'
    ## Numpy array ##
    power_plants = np.empty((0,2), str)
        
    # Retrieve all country urls #
    response = requests.get(root_url + '/energy/5575-fossil-fuels-energy', headers=headers)
    soup = BeautifulSoup(response.text, 'html.parser') 
    country_urls = [root_url + url['href'] 
                    for url in (soup.find('div',  {'itemprop':'articleBody'})
                                    .find_all('a', {'href':True}))]
    country_urls = [country_url 
                    for country_url in country_urls
                    if re.search('country-territories', country_url)]
    clean_responses(response, soup)
    
    # Retrieve all thermal power plant urls #
    for country_url in country_urls:
        try:
            response = requests.get(country_url, headers=headers)
        except Exception:
            pass
        else:
            while True:
                ## Parse web contents using BeautifulSoup ##
                soup = BeautifulSoup(response.text, 'html.parser')
                rows = soup.find_all('td', {'class':'list-title'})
                ## Append power plants into array ##
                plant_urls = [[row.text.strip(), root_url + row.find('a')['href']] 
                              for row in rows 
                              if not re.search(r'shutdown', row.text, re.IGNORECASE)]
                if len(plant_urls) > 0:
                    power_plants = np.vstack((power_plants, np.array(plant_urls)))
                ## Free up memory ##
                clean_responses(response, soup)
                ## Check for subsequent page counter ##
                if soup.find('a', {'title':'Next'}):
                    ### Proceed to next page ###
                    next_url = root_url + soup.find('a', {'title':'Next'})['href']
                    response = requests.get(next_url, headers=headers)   
                else:
                    ### End of page - Break out of While loop ###
                    break
    
    # Convert array into DataFrame #
    power_df = pd.DataFrame(power_plants, columns = ['THERMAL_PP', 'WEBLINK'])
    power_df.to_excel(download_path + '\\IA_PP_LANDINGS.xlsx', index=False)
                
    return power_df


def get_IA_plants(power_df):
    ''' 
    Retrieve the details for all thermal power plants in IA.
    Parameter:
        coal_mines : DataFrame containing IA power plants and their local weblinks
    Return:
        mines_data : DataFrame containing IA power plants and their relevant details
    '''
    # Initialise parameters #
    ## Weblinks ##
    weblinks = list(power_df['WEBLINK'])
    ## Empty list #
    tpp_data = []
    
    # Loop through each target weblink and get relevant company weblinks # 
    func_get_plants = partial(get_IA_plant, power_df=power_df)
    with ThreadPoolExecutor() as executor:
        results = executor.map(func_get_plants, weblinks)
        for result in results:
            if len(result) != 0:
                tpp_data.append(result)
    
    # Convert data into DataFrame and preprocess the data #
    tpp_data = pd.DataFrame(tpp_data)
    ## Preprocess data ##
    tpp_data['PLANT STATUS'] = 'OPERATING'
    tpp_data.loc[tpp_data['KIND OF FUEL'].isnull(), 'KIND OF FUEL'] = \
    tpp_data.loc[tpp_data['KIND OF FUEL'].isnull(), 'KIND OF COAL']
    tpp_data.loc[tpp_data['KIND OF FUEL'].isnull(), 'KIND OF FUEL'] = \
    tpp_data.loc[tpp_data['KIND OF FUEL'].isnull(), 'ORIGIN OF FUEL'] 
    tpp_data.loc[(tpp_data['POWER CAPACITY'] == 'None') & (tpp_data['HEAT CAPACITY'].notnull()), 'POWER CAPACITY'] = \
    tpp_data.loc[(tpp_data['POWER CAPACITY'] == 'None') & (tpp_data['HEAT CAPACITY'].notnull()), 'HEAT CAPACITY']
    tpp_data.loc[(tpp_data['POWER CAPACITY'].isnull()) & (tpp_data['HEAT CAPACITY'].notnull()), 'POWER CAPACITY'] = \
    tpp_data.loc[(tpp_data['POWER CAPACITY'].isnull()) & (tpp_data['HEAT CAPACITY'].notnull()), 'HEAT CAPACITY']
    tpp_data.loc[tpp_data['OWNER'].isnull(), 'OWNER'] = \
    tpp_data.loc[tpp_data['OWNER'].isnull(), 'OWNERS']
    ## Rearrange and rename columns ##
    tpp_data = tpp_data[['THERMAL PP', 'LOCATION', 'PLANT STATUS',
                         'TYPE', 'KIND OF FUEL', 'POWER CAPACITY',
                         'OWNER', 'SHAREHOLDERS','WEBLINK', 
                         'PUBLISHING DATE']]
    tpp_data.columns = ['THERMAL_PP', 'LOCATION', 'PLANT_STATUS',
                         'PLANT_TYPE', 'FUEL_TYPE', 'POWER CAPACITY',
                         'OWNER', 'PARENT_COMPANY', 'WEBLINK', 
                         'PUBLISHING DATE']
    
    # Download processed DataFrame #
    tpp_data.to_excel(download_path + '\\IA_THERMAL_PP.xlsx', 
                        sheet_name='THERMAL_PP',
                        index=False)

    return tpp_data


def get_IA_plant(weblink, power_df):
    ''' 
    Retrieve the details for target coal mine in IA.
    Parameters:
        weblink    : Target local weblink that contain power plant details
        power_df   : DataFrame containing local weblinks of thermal power plants
    Return:
        tpp_data   : DataFrame containing thermal power plants and their relevant details
    '''
    # Initialise parameters #
    ## Name of thermal power plant ##
    tpp = power_df.loc[power_df['WEBLINK'] == weblink, 'THERMAL_PP'].iloc[0]
    ## Dictionary ##
    tpp_data = {}

    try:
        # Send GET request #
        response = requests.get(weblink, headers=headers)
    except Exception:
        # Print status message #
        print('Failed to access weblink: %s' % weblink)
    else:
        # Apply Beautiful Soup to scrape relevant details #
        soup = BeautifulSoup(response.text, 'html.parser')
        ## Publishing Date ##
        date_text = soup.find('time', {'itemprop': 'dateModified'}).text.strip()
        date = re.search(r'\d+\s?\w+\s?\d+', date_text).group(0)
        date = dt.datetime.strptime(date, '%d %B %Y').date()
        ## Country ##
        country = (soup.find('dd', {'class': 'parent-category-name'})
                       .find('a').text.strip())
        ## Other plant details ##
        if soup.find('div', {'itemprop':'articleBody'}).find_all('li') and \
            len(soup.find('div', {'itemprop':'articleBody'}).find_all('li')) > 1:
            paragraphs = soup.find('div', {'itemprop':'articleBody'}).find_all('li')
            for paragraph in paragraphs:
                paragraph_text = paragraph.text.split(':')
                if len(paragraph_text) == 2:
                    tpp_data[paragraph_text[0].strip().upper()] = paragraph_text[-1].strip()
                elif len(paragraph_text) == 1 and 'Contact' not in paragraph_text:
                    tpp_data[paragraph_text[0].strip().upper()] = ''
                elif len(paragraph_text) == 1 and 'Contact' in paragraph_text:
                ### Break FOR loop at CONTACT paragraph ###
                    break
            
        elif soup.find('div', {'itemprop':'articleBody'}).find_all('p')[1:]:
            paragraphs = soup.find('div', {'itemprop':'articleBody'}).find_all('p')[1:]
            ### Differentiation by HTML tags ###
            if paragraphs[0].find('br'):
                segments = paragraphs[0].find_all('strong')
                for segment in segments:
                    paragraph_text = segment.text.split(':')
                    if len(paragraph_text) == 4:
                        tpp_data[paragraph_text[0].strip().upper()] = paragraph_text[1].strip()
                        tpp_data[paragraph_text[2].strip().upper()] = paragraph_text[-1].strip()
                    elif len(paragraph_text) == 2 or len(paragraph_text) == 3:
                        tpp_data[paragraph_text[0].strip().upper()] = paragraph_text[-1].strip()
                    elif len(paragraph_text) == 1 and 'Contact' not in paragraph_text:
                        tpp_data[paragraph_text[0].strip().upper()] = ''
                    elif len(paragraph_text) == 1 and 'Contact' in paragraph_text:
                    ### Break FOR loop at CONTACT paragraph ###
                        break
            else:    
                for paragraph in paragraphs:
                    paragraph_text = paragraph.text.split(':')
                    if len(paragraph_text) == 4:
                        tpp_data[paragraph_text[0].strip().upper()] = paragraph_text[1].strip()
                        tpp_data[paragraph_text[2].strip().upper()] = paragraph_text[-1].strip()
                    elif len(paragraph_text) == 2 or len(paragraph_text) == 3:
                        tpp_data[paragraph_text[0].strip().upper()] = paragraph_text[-1].strip()
                    elif len(paragraph_text) == 1:
                        tpp_data[paragraph_text[0].strip().upper()] = ''
                    ### Break FOR loop at first empty paragraph ###
                    if not paragraph.find('strong'):
                        break
        
        else:
            ### Print status message ###
            print('Plant details could not be found: %s' % weblink)
        
        # Append additional mine details #
        if len(tpp_data) > 0:
            tpp_data['THERMAL PP'] = tpp
            tpp_data['WEBLINK'] = weblink
            tpp_data['LOCATION'] = country
            tpp_data['PUBLISHING DATE'] = date
        
        # Free up memory #
        clean_responses(response, soup)

    return tpp_data

# Execution #
## String containing local download path ##
download_path = os.environ['USERPROFILE'] + r'\Dropbox\ESG Research\Energy Extractives\Industry About'
## Headers ##
headers = {'User-Agent': 'Mozilla/5.0 (Macintosh; Intel Mac OS X 10_11_6)' + \
           'AppleWebKit/537.36 (KHTML, like Gecko) Chrome/61.0.3163.100 Safari/537.36'}