# Import Python libraries #
from bs4 import BeautifulSoup
from concurrent.futures import ThreadPoolExecutor
from functools import partial
import datetime as dt
import gc
import Levenshtein as lev
import numpy as np
import os
import pandas as pd
import re
import requests
from selenium import webdriver
from selenium.webdriver import DesiredCapabilities
from selenium.webdriver.common.by import By
from selenium.webdriver.firefox.options import Options
from selenium.webdriver.support.ui import WebDriverWait
from selenium.webdriver.support import expected_conditions as EC
import time


######################################################################################################################################
''' 
##########          ########## 
            LOGIN 
##########          ##########  
'''
    
def start_webpage(log_path, download_path=None, mime=None):
    ''' 
    Initiate a Firebox browser session.
    Parameters:
        log_path      : String containing the filepath of the geckodriver log file
        download_path : String containing the local download filepath.
                        Default value is None
        mime          : Mime file type(s) that would be downloaded.
                        Default value is None
    Return:
        browser       : Instantiated Firefox browser
    '''
    # Initiate a Firefox browser session #
    profile = webdriver.FirefoxProfile()

    PROXY_HOST = '12.12.12.123'
    PROXY_PORT = '1234'
    profile.set_preference('network.proxy.type', 1)
    profile.set_preference('network.proxy.http', PROXY_HOST)
    profile.set_preference('network.proxy.http_port', int(PROXY_PORT))
    profile.set_preference('general.useragent.override', 'Mozilla/5.0' + \
                           '(Macintosh; Intel Mac OS X 10_11_6)' + \
                           'AppleWebKit/537.36 (KHTML, like Gecko)' + \
                           ' Chrome/61.0.3163.100 Safari/537.36')
    profile.set_preference('devtools.jsonview.enabled', False)
    profile.set_preference('dom.webdriver.enabled', False)
    profile.set_preference('useAutomationExtension', False)
    profile.set_preference('accessibility.blockautorefresh', True)
    profile.set_preference('intl.accept_languages', 'en-US, en')
    profile.native_events_enabled = True
    profile.update_preferences()
    desired = DesiredCapabilities.FIREFOX
    
    if download_path != None:
        profile.set_preference('browser.download.folderList', 2)
        profile.set_preference('browser.download.dir', download_path)
        profile.set_preference('pdfjs.disabled', True)
        if mime != None:
            profile.set_preference('browser.helperApps.neverAsk.saveToDisk', mime)
        else:
            raise ValueError('Please provide MIME type in the argument!')
                       
    options = Options()
    # options.add_argument('--headless')
    browser = webdriver.Firefox(firefox_profile=profile,
                                desired_capabilities=desired,
                                options=options,
                                service_log_path=log_path)
    
    return browser

######################################################################################################################################
''' 
##########                  ########## 
            WEBSCRAPPING 
##########                  ##########  
'''

def chunks(lst, n):
    '''
    Generate successive n-sized chunks from list.
    Parameters:
        lst           : List containing the elements to be broken into chunks
        n             : Size of each chunk
    Return:
        chunked_list  : List containing n-sized chunks
    '''
    chunked_list = [lst[i:i + n] for i in range(0, len(lst), n)]
    
    return chunked_list


def clean_responses(response, soup=None):
    '''
    Clear all the the responses to free up memory. 
    Parameters:
        response      : Response from GET requests
        soup          : Parsed BeautifulSoup     
    '''
    # Close response #
    response.close()
    response = None
    
    # Decompose soup if soup is valid #
    if soup is not None:
        soup.decompose()
        
    # Clear all the waste #
    gc.collect()
    

def get_GEM_landings():
    ''' 
    Retrieve the main landing page for all coal mines in Global Energy Monitor (GEM).
    Return:
        coal_mines : DataFrame containing local weblinks of company mines
    '''
    # Initiate parameters #
    ## Browser ##
    browser = start_webpage(download_path + '\\geckodriver.log')
    ## GEM Coal Mine websites ##
    urls = ['https://www.gem.wiki/w/index.php?title=Category:Coal_mines',
            'https://www.gem.wiki/Category:Existing_coal_mines_in_India',
            'https://www.gem.wiki/Category:Existing_coal_mines_in_the_United_States',
            'https://www.gem.wiki/Category:Existing_coal_mines_in_Australia',
            'https://www.gem.wiki/Category:Coal_mines_in_Russia',
            'https://www.gem.wiki/Category:Coal_mines_in_Indonesia',
            'https://www.gem.wiki/Category:Existing_coal_mines_in_Turkey',
            'https://www.gem.wiki/Category:Existing_coal_mines_in_China']
    ## Numpy arrays ##
    coal_mines = np.empty((0,2), str)
    chinese_ents = np.empty((0,2), str)
    
    # Loop through each url #
    for url in urls:
        ## Boolean counter ##
        next_page = True
        
        ## Send GET request to GEM Coal Mine website ##
        browser.get(url)
        time.sleep(2)
        
        ## Loop through each page to Scrape the local weblinks of all coal mines ##
        while next_page:
            ## Apply Beautiful Soup to get local weblinks of all coal mines ##
            soup = BeautifulSoup(browser.page_source, 'html.parser')
            groups = soup.find_all('div', {'class': 'mw-category-group'}) 
            entities = [[ent.text.strip(), 'https://www.gem.wiki' + ent.find('a')['href']] 
                        for group in groups 
                        for ent in group.find_all('li')]
            
            ## Append list to array ##
            coal_mines = np.vstack((coal_mines, np.array(entities)))
            
            try:
                next_button = browser.find_element_by_link_text('next page')
            except Exception:
                ## Break out of while loop ##
                next_page = False
            else:
                ## Navigate to next page ##
                next_button.click()
                time.sleep(2)
    
    # Close browser #
    browser.close()
    
    # Convert array into DataFrame #
    coal_mines = pd.DataFrame(coal_mines, columns = ['COAL_MINE', 'WEBLINK'])
    ## Drop irrelevant records ##
    coal_mines.drop_duplicates(subset=['WEBLINK'], inplace=True)
    drop_titles = ['Existing', 'Major', 'Propose']
    for drop_title in drop_titles:
        coal_mines.drop((coal_mines.loc[coal_mines['COAL_MINE'].str
                                    .startswith(drop_title)].index),
                        inplace=True)
    coal_mines.replace('nan', np.nan, inplace=True)
    
    # Get corresponding English annotations for Chinese coal mines #
    ## Filter Chinese entities ##
    chinese_entities = coal_mines.loc[~coal_mines['COAL_MINE'].str.contains(r'[A-Za-z0-9]')]
    chinese_weblinks = list(chinese_entities['WEBLINK'])
    coal_mines.drop(chinese_entities.index, inplace=True)
    ## Loop through each Chinese weblink and get corresponding English annotations ##
    with ThreadPoolExecutor() as executor:
        results = executor.map(get_english_landings, chinese_weblinks)
        for result in results:
            if len(result) != 0:
                chinese_ents = np.vstack((chinese_ents, result))
    
    # Convert array into DataFrame #
    chinese_mines = pd.DataFrame(chinese_ents, columns = ['COAL_MINE', 'WEBLINK'])
    coal_mines = coal_mines.append(chinese_mines, ignore_index=True)
    
    # Download DataFrame #
    coal_mines.to_excel(download_path + '\\GEM_LANDINGS.xlsx', 
                        sheet_name='MINES', 
                        index=False)
        
    return coal_mines    


def get_english_landings(chinese_weblink):
    ''' 
    Retrieve the English details for all Chinese coal mines in GEM.
    Parameter:
        chinese_weblink  : Target Chinese weblink
    Return:
        chinese_entities : Numpy array containing Chinese coal mines 
                           and their English details
    '''
    # Initiate parameter #
    chinese_ents = np.empty((0,2), str)
    
    # Send GET request to target Chinese weblink #
    response = requests.get(chinese_weblink, headers=headers)
    soup = BeautifulSoup(response.text, 'html.parser')
    
    # Retrieve relevant English annotations #
    try:
        ent = soup.find('div', {'class':'mw-parser-output'}).find('p').find('a')
    except Exception:
        print('English version could not be found: %s' % chinese_weblink)
    else:
        chinese_ent = np.array([[ent.text.strip(), 'https://www.gem.wiki' + ent['href']]])
        chinese_ents = np.vstack((chinese_ents, chinese_ent))
    
    # Free up memory #
    clean_responses(response, soup)
    
    return chinese_ents
    

def get_GEM_mines(coal_mines):
    ''' 
    Retrieve the details for all coal mines in GEM.
    Parameter:
        coal_mines : DataFrame containing GEM coal mines and their local weblinks
    Return:
        mines_data : DataFrame containing GEM coal mines and their relevant details
    '''
    # Initialise parameters #
    ## Weblinks ##
    weblinks = list(coal_mines['WEBLINK'])
    ## Empty list #
    mine_data = []
    
    # Loop through each target weblink and get relevant company weblinks # 
    func_get_mines = partial(get_GEM_mine, coal_mines=coal_mines)
    with ThreadPoolExecutor() as executor:
        results = executor.map(func_get_mines, weblinks)
        for result in results:
            if len(result) != 0:
                mine_data.append(result)
    
    # Convert data into DataFrame and preprocess the data #
    mines_data = pd.DataFrame(mine_data)
    mines_data = preprocess_GEM(mines_data)
    
    # Download processed DataFrame #
    mines_data.to_excel(download_path + '\\GEM_COAL_MINES.xlsx', 
                        sheet_name='GEM_MINES',
                        index=False)
    
    return mines_data


def get_GEM_mine(weblink, coal_mines):
    ''' 
    Retrieve the details for target coal mine in GEM.
    Parameters:
        weblink    : Target local weblink that contain mine details
        coal_mines : DataFrame containing local weblinks of company mines
    Return:
        mine_data : DataFrame containing coal mines and their relevant details
    '''
    # Initialise parameters #
    ## Name of coal mine ##
    mine = coal_mines.loc[coal_mines['WEBLINK'] == weblink, 'COAL_MINE'].iloc[0]
    ## Dictionary ##
    mine_data = {}

    try:
        # Send GET request #
        response = requests.get(weblink, headers=headers)
    except Exception:
        # Print status message #
        print('Failed to access weblink: %s' % weblink)
    else:
        # Apply Beautiful Soup to scrape relevant details #
        soup = BeautifulSoup(response.text, 'html.parser')
        if soup.find('h2', string=re.compile(r'(Mine|Project) (Data|Details)', flags=re.IGNORECASE)):
            rows = (soup.find('h2', 
                              string=re.compile(r'(Mine|Project) (Data|Details)', flags=re.IGNORECASE))
                    .find_next('ul')
                    .find_all('li'))
            mine_details = [[re.sub(':', '', row.find('b').text).strip().upper(), 
                             (re.sub(r'\[[0-9]+\]', '', 
                                    re.sub(':', '', re.split(row.find('b').text, row.text)[-1])).strip())] 
                            for row in rows
                            if row.find('b')]
            ## Store mine details in dictionary ##
            for key, value in mine_details:
                mine_data[key] = value
            mine_data['COAL MINE'] = mine
            mine_data['WEBLINK'] = weblink
            if re.search(r'\d+\s?\w+\s?\d+', soup.find('div', {'id':'footer-info'}).find('div').text):
                date = re.search(r'\d+\s?\w+\s?\d+', (soup.find('div', {'id':'footer-info'})
                                                          .find('div').text).group(0))
                date = dt.datetime.strptime(date, '%d %B %Y').date()
                mine_data['PUBLISHING DATE'] = date
            else:
                mine_data['PUBLISHING DATE'] = ''
        
        else:
            # Print status message #
            print('Mine details could not be found: %s' % weblink)
            
        # Free up memory #
        clean_responses(response, soup)

    return mine_data
    

def preprocess_GEM(mines_data):
    ''' 
    Perform preprocessing on GEM coal mines.
    Parameter:
        mines_data : Unprocessed DataFrame containing GEM coal mines 
    Return:
        mines_data : Processed DataFrame containing GEM coal mines
    '''
    # Replace irrelevant data #
    mines_data.replace('', np.nan, inplace=True)
    mines_data.replace('TBD', np.nan, inplace=True)
    
    # Replace OWNER nan values with SPONSOR / OPERATOR values # 
    mines_data.loc[mines_data['OWNER'].isnull(), 'OWNER'] = \
        mines_data.loc[mines_data['OWNER'].isnull(), 'SPONSOR']
    mines_data.loc[mines_data['OWNER'].isnull(), 'OWNER'] = \
        mines_data.loc[mines_data['OWNER'].isnull(), 'OPERATOR']
    
    # Replace PARENT COMPANY nan values with CONTROLLER values #
    mines_data.loc[mines_data['PARENT COMPANY'].isnull(), 'PARENT COMPANY'] = \
        mines_data.loc[mines_data['PARENT COMPANY'].isnull(), 'CONTROLLER']
    
    # Replace MINE STATUS nan values with STATUS values #
    mines_data.loc[mines_data['MINE STATUS'].isnull(), 'MINE STATUS'] = \
        mines_data.loc[mines_data['MINE STATUS'].isnull(), 'STATUS']
    
    # Replace MINE TYPE nan values with MINING METHOD / TYPE values # 
    mines_data.loc[mines_data['MINE TYPE'].isnull(), 'MINE TYPE'] = \
        mines_data.loc[mines_data['MINE TYPE'].isnull(), 'MINING METHOD']
    mines_data.loc[mines_data['MINE TYPE'].isnull(), 'MINE TYPE'] = \
        mines_data.loc[mines_data['MINE TYPE'].isnull(), 'MINING TYPE']    
    
    # Replace COAL TYPE nan values with TYPE OF COAL values #
    mines_data.loc[mines_data['COAL TYPE'].isnull(), 'COAL TYPE'] = \
        mines_data.loc[mines_data['COAL TYPE'].isnull(), 'TYPE OF COAL']
        
    # Reserves #
    ## Replace PROVEN RESERVES nan values with all related RESERVES values ##
    columns = [column for column in list(mines_data.columns) 
                if re.search('RESERVE', column) and 
                not re.search(r'PROVEN|TOTAL|RECOVERABLE|ESTIMATES', column)]
    for column in columns:
        mines_data.loc[mines_data['PROVEN RESERVES'].isnull(), 'PROVEN RESERVES'] = \
        mines_data.loc[mines_data['PROVEN RESERVES'].isnull(), column]
    ## Replace RECOVERABLE RESERVES nan values with all related RECOVERABLE RESERVES values ##
    mines_data.loc[mines_data['RECOVERABLE RESERVES'].isnull(), 'RECOVERABLE RESERVES'] = \
        mines_data.loc[mines_data['RECOVERABLE RESERVES'].isnull(), 'RECOVERABLE RESERVES (MT)']
    ## Replace TOTAL RESERVES nan values with all related TOTAL RESERVES values ##
    mines_data.loc[mines_data['TOTAL RESERVES'].isnull(), 'TOTAL RESERVES'] = \
        mines_data.loc[mines_data['TOTAL RESERVES'].isnull(), 'RESERVE ESTIMATES']
    
    # Production Capacity #
    ## Replace PRODUCTION CAPACITY nan values with all related PRODUCTION values ##
    columns = [column for column in list(mines_data.columns) 
                if re.search(r'PRODUCTION|CAPACITY', column) and 
                not re.search(r'SHORT TONS|PROPOSED|\d+', column)]
    for column in columns:
        mines_data.loc[mines_data['PRODUCTION CAPACITY'].isnull(), 'PRODUCTION CAPACITY'] = \
        mines_data.loc[mines_data['PRODUCTION CAPACITY'].isnull(), column]
    ## Replace 2009 PRODUCTION nan values with all related 2009 PRODUCTION values ##
    mines_data.loc[mines_data['2009 PRODUCTION (SHORT TONS)'].isnull(), '2009 PRODUCTION (SHORT TONS)'] = \
        mines_data.loc[mines_data['2009 PRODUCTION (SHORT TONS)'].isnull(), '2009 PRODUCTION']   
    
    # Create new column for Year of Production #
    mines_data['PRODUCTION YEAR'] = np.nan
    columns = [column for column in list(mines_data.columns) 
                if re.search(r'\d{4} PRODUCTION \(SHORT TONS\)', column)]
    for column in columns:
        mines_data.loc[mines_data[column].notnull(), 'PRODUCTION YEAR'] = \
            re.search(r'\d{4}', column).group(0)
    ## Consolidate PRODUCTION (SHORT TONS) values ##
    for column in columns:
        mines_data.loc[mines_data['PRODUCTION (SHORT TONS)'].isnull(), 'PRODUCTION (SHORT TONS)'] = \
        mines_data.loc[mines_data['PRODUCTION (SHORT TONS)'].isnull(), column] 
    
    # Select relevant columns for output data # 
    mines_data = mines_data[['COAL MINE', 'LOCATION', 'MINE STATUS',
                              'MINE TYPE', 'COAL TYPE', 'PRODUCTION CAPACITY',
                              'PRODUCTION (SHORT TONS)', 'PRODUCTION YEAR', 'PROVEN RESERVES',
                              'RECOVERABLE RESERVES', 'TOTAL RESERVES', 'OWNER', 
                              'PARENT COMPANY', 'WEBLINK', 'PUBLISHING DATE']]
    mines_data.columns = [re.sub(' ', '_', column) for column in list(mines_data.columns)]
    mines_data.drop_duplicates(subset=['WEBLINK'], inplace=True)
    
    return mines_data
    

def get_IA_landings():
    ''' 
    Retrieve the main landing page for all coal mines in Industry About (IA).
    Return:
        coal_mines : DataFrame containing local weblinks of company mines
    '''
    # Initiate parameters #
    ## Root IA url ##
    root_url = 'https://www.industryabout.com'
    ## Main IA url ##
    main_url = 'https://www.industryabout.com/component/search/?' + \
               'searchword=Coal%20mining&ordering=newest&' + \
               'searchphrase=exact&limit=50&areas[0]=categories'
    ## Numpy array ##
    coal_mines = np.empty((0,2), str)
        
    # Retrieve all country urls #
    response = requests.get(main_url, headers=headers)
    soup = BeautifulSoup(response.text, 'html.parser') 
    country_urls = [root_url + url['href'] 
                    for url in soup.find('dl',  {'class':'search-results'}).find_all('a', {'href':True})]
    clean_responses(response, soup)
    
    # Retrieve all mine urls #
    for country_url in country_urls:
        response = requests.get(country_url, headers=headers)
        next_page = True
        ## Loop through each page to retrieve local weblinks ##
        while next_page:
            soup = BeautifulSoup(response.text, 'html.parser')
            rows = soup.find_all('td', {'class':'list-title'})
            mine_urls = [[row.text.strip(), root_url + row.find('a')['href']] 
                         for row in rows 
                         if not re.search(r'wash|plant|terminal', row.text, re.IGNORECASE)]
            print(country_url, len(mine_urls))
            if len(mine_urls) > 0:
                coal_mines = np.vstack((coal_mines, np.array(mine_urls)))
            
            ## Locate next page button ##
            next_ = soup.find('a', {'title':'Next'})
            
            ### Free up memory ###
            clean_responses(response, soup)
            
            if next_:
                ### Proceed to next page ###
                next_url = root_url + next_['href']
                response = requests.get(next_url, headers=headers)                
            else:
                ### Break out of while loop ###
                next_page = False
    
    # Convert array into DataFrame #
    coal_mines = pd.DataFrame(coal_mines, columns = ['COAL_MINE', 'WEBLINK'])
    coal_mines.to_excel(download_path + '\\IA_LANDINGS.xlsx', index=False)
                
    return coal_mines


def get_IA_mines(coal_mines):
    ''' 
    Retrieve the details for all coal mines in IA.
    Parameter:
        coal_mines : DataFrame containing IA coal mines and their local weblinks
    Return:
        mines_data : DataFrame containing IA coal mines and their relevant details
    '''
    # Initialise parameters #
    ## Weblinks ##
    weblinks = list(coal_mines['WEBLINK'])
    ## Empty list #
    mine_data = []
    
    # Loop through each target weblink and get relevant company weblinks # 
    func_get_mines = partial(get_IA_mine, coal_mines=coal_mines)
    with ThreadPoolExecutor() as executor:
        results = executor.map(func_get_mines, weblinks)
        for result in results:
            if len(result) != 0:
                mine_data.append(result)
    
    # Convert data into DataFrame and preprocess the data #
    mines_data = pd.DataFrame(mine_data)
    ## Preprocess data ##
    mines_data['MINE STATUS'] = 'OPERATING'
    mines_data.loc[mines_data['ACTIVIY UNTIL'].notnull(), 'MINE STATUS'] = 'CLOSED'
    mines_data.loc[mines_data['PRODUCTS'].isnull(), 'PRODUCTS'] = \
        mines_data.loc[mines_data['PRODUCTS'].isnull(), 'COMMODITIES']
    mines_data['PUBLISHING DATE'] = [time.date() for time in list(mines_data['PUBLISHING DATE'])]
    ## Rearrange and rename columns ##
    mines_data = mines_data[['COAL MINE', 'LOCATION', 'MINE STATUS',
                             'TYPE', 'PRODUCTS', 'ANNUAL PRODUCTION',
                             'OWNER', 'SHAREHOLDERS','WEBLINK', 
                             'PUBLISHING DATE']]
    mines_data.columns = ['COAL MINE', 'LOCATION', 'MINE STATUS',
                          'MINE TYPE', 'COAL TYPE', 'PRODUCTION CAPACITY',
                          'OWNER', 'PARENT COMPANY', 'WEBLINK', 
                          'PUBLISHING DATE']
    mines_data.columns = [re.sub(' ', '_', column) for column in list(mines_data.columns)]
    
    # Download processed DataFrame #
    mines_data.to_excel(download_path + '\\IA_COAL_MINES.xlsx', 
                        sheet_name='IA_MINES',
                        index=False)

    return mines_data


def get_IA_mine(weblink, coal_mines):
    ''' 
    Retrieve the details for target coal mine in IA.
    Parameters:
        weblink    : Target local weblink that contain mine details
        coal_mines : DataFrame containing local weblinks of company mines
    Return:
        mine_data : DataFrame containing coal mines and their relevant details
    '''
    # Initialise parameters #
    ## Name of coal mine ##
    mine = coal_mines.loc[coal_mines['WEBLINK'] == weblink, 'COAL_MINE'].iloc[0]
    ## Dictionary ##
    mine_data = {}

    try:
        # Send GET request #
        response = requests.get(weblink, headers=headers)
    except Exception:
        # Print status message #
        print('Failed to access weblink: %s' % weblink)
    else:
        # Apply Beautiful Soup to scrape relevant details #
        soup = BeautifulSoup(response.text, 'html.parser')
        ## Publishing Date ##
        date_text = soup.find('time', {'itemprop': 'dateModified'}).text.strip()
        date = re.search(r'\d+\s?\w+\s?\d+', date_text).group(0)
        date = dt.datetime.strptime(date, '%d %B %Y').date()
        ## Country ##
        country = (soup.find('dd', {'class': 'parent-category-name'})
                       .find('a').text.strip())
        ## Other mine details ##
        if soup.find('div', {'itemprop':'articleBody'}).find_all('li'):
            paragraphs = soup.find('div', {'itemprop':'articleBody'}).find_all('li')
            for paragraph in paragraphs:
                paragraph_text = paragraph.text.split(':')
                if len(paragraph_text) == 2:
                    mine_data[paragraph_text[0].strip().upper()] = paragraph_text[-1].strip()
                elif len(paragraph_text) == 1 and 'Contact' not in paragraph_text:
                    mine_data[paragraph_text[0].strip().upper()] = ''
                elif len(paragraph_text) == 1 and 'Contact' in paragraph_text:
                ### Break FOR loop at CONTACT paragraph ###
                    break
            
        elif soup.find('div', {'itemprop':'articleBody'}).find_all('p')[1:]:
            paragraphs = soup.find('div', {'itemprop':'articleBody'}).find_all('p')[1:]
            ### Differentiation by HTML tags ###
            if paragraphs[0].find('br'):
                segments = paragraphs[0].find_all('strong')
                for segment in segments:
                    paragraph_text = segment.text.split(':')
                    if len(paragraph_text) == 2:
                        mine_data[paragraph_text[0].strip().upper()] = paragraph_text[-1].strip()
                    elif len(paragraph_text) == 1 and 'Contact' not in paragraph_text:
                        mine_data[paragraph_text[0].strip().upper()] = ''
                    elif len(paragraph_text) == 1 and 'Contact' in paragraph_text:
                    ### Break FOR loop at CONTACT paragraph ###
                        break
            else:    
                for paragraph in paragraphs:
                    paragraph_text = paragraph.text.split(':')
                    if len(paragraph_text) == 2:
                        mine_data[paragraph_text[0].strip().upper()] = paragraph_text[-1].strip()
                    elif len(paragraph_text) == 1:
                        mine_data[paragraph_text[0].strip().upper()] = ''
                    ### Break FOR loop at first empty paragraph ###
                    if not paragraph.find('strong'):
                        break
        
        else:
            ### Print status message ###
            print('Mine details could not be found: %s' % weblink)
        
        # Append additional mine details #
        if len(mine_data) > 0:
            mine_data['COAL MINE'] = mine
            mine_data['WEBLINK'] = weblink
            mine_data['LOCATION'] = country
            mine_data['PUBLISHING DATE'] = date
        
        # Free up memory #
        clean_responses(response, soup)

    return mine_data

######################################################################################################################################
''' 
##########                  ########## 
           ENTITY MATCHING 
##########                  ##########  
'''

def get_entities(filepath, sheet_name='CONSO_OWNERS'):
    '''
    Get list of entities from Excel data. 
    Parameter:
        filepath      : Input Excel filepath (including filename)
        sheet_name    : Target Excel sheet. Default value is 'CONSO_OWNERS'
    Return:
        entities      : Output DataFrame containing the list of entities
        entities_list : List containing all the entities for subsequent matching
    '''    
    # Initiate parameters #
    owners_list = []
    parents_list = []
    
    # Load DataFrame #
    data = pd.read_excel(filepath, sheet_name=sheet_name)
    owners_data = data.dropna(subset=['OWNER'])
    parents_data = data.dropna(subset=['PARENT_COMPANY'])
    
    # Populate rows for each owner #
    owners = list(owners_data['OWNER'])
    for owner in owners:
        mine = owners_data.loc[owners_data['OWNER'] == owner, 'COAL_MINE'].iloc[0]
        ents = owner.split(',')
        for ent in ents:
            owners_list.append([mine, ent.strip()])
            
    # Populate rows for each parent #
    parents = list(parents_data['PARENT_COMPANY'])
    for parent in parents:
        mine = parents_data.loc[parents_data['PARENT_COMPANY'] == parent, 'COAL_MINE'].iloc[0]
        ents = parent.split(',')
        for ent in ents:
            if re.search(r'\%', ent):
                parents_list.append([mine, 
                                     re.sub(r'(.+)\(\d+\.?\d*\%\)', r'\1', ent).strip(), 
                                     re.sub(r'.+\((\d+\.?\d*\%)\)', r'\1', ent).strip()])
            else:
                parents_list.append([mine, ent.strip(), np.nan])
    
    # Convert both lists into DataFrames #
    owners = pd.DataFrame(owners_list, columns=['COAL_MINE', 'OWNERS'])
    parents = pd.DataFrame(parents_list, columns=['COAL_MINE', 'PARENTS', 'MINE_STAKE'])
    
    # Merge all DataFrames #
    entities = pd.merge(data, owners, how='left', on='COAL_MINE')
    entities = pd.merge(entities, parents, how='left', on='COAL_MINE')
    
    # Rename columns and drop duplicates #
    entities = entities[['COAL_MINE', 'OWNERS', 'PARENTS', 'MINE_STAKE']]
    entities.columns = ['COAL_MINE', 'MINE_OWNER', 'MINE_PARENT', 'MINE_PARENT_STAKE']
    entities.drop_duplicates(inplace=True)
    entities.replace('nan', np.nan, inplace=True)
    
    # Download processed DataFrame #
    entities.to_excel(download_path + '\\COAL_ENTITIES.xlsx',
                      sheet_name = 'ENTITIES',
                      index=False)
    
    # Consolidate all entities into list #
    entities_list = [owner for mine, owner in owners_list] + \
                    [parent for mine, parent, stake in parents_list]
    entities_list = list(set(entities_list))
    
    return entities, entities_list
    
    
def preprocess_names(df, abb_patterns):
    '''
    Preprocess company names based on input DataFrame. 
    Parameter:
        df            : Input DataFrame containing the company names to be preprocessed
        abb_patterns  : String containing the company abbreviations to be eliminated
    Return:
        df            : Output DataFrame containing the company names after preprocessing
    '''
    # Initiate parameter #
    companies_new = []
    
    # Review all columns and identify whether 'Company' column is present #
    try:
        company_column = [column for column in df.columns if column == 'Company'][0]
    except Exception:
        # Print error status message #
        print('The columns do not contain a Company column. Please review dataset and '
              'ensure the Company column is present.')
    else:
        ## Change company name to upper case ##
        df[company_column] = df[company_column].str.upper() 
        ## Create a list for company names ##
        companies =  list(df[company_column])
        ## Iterate over company_list to remove abb_patterns ##
        for company in companies:
            for match in re.finditer(abb_patterns, company):
                pattern = match.group(0)
                if pattern is not None:
                    company = re.sub(r'[^\w\s]', ' ', re.sub(r'\b%s\b' % pattern, '', company)).strip() 
            company = re.sub(r'\bTHE\b', ' ', company).strip()
            company = re.sub(r'[^\w\s]', ' ', company).strip()
            companies_new.append(re.sub(r'\s{2,}', ' ', company))
        ## Create new column in df ##
        df['COMPANY_NEW'] = companies_new
        
        return df
    

def get_closest_match(company_df_1, company_df_2, minimum_ratio=0.7, top_few=3):
    '''
    Computes the Levenshtein distance between 2 strings 
    Parameters:
        company_df_1    : DataFrame containing the first set of company names (i.e. SBSU_companies)
        company_df_2    : DataFrame containing the second set of company names
        minimum_ratio   : Minimum threshold to be met for similarity matching
        top_few         : Maximum number of records, arranged in descending ratio
    Return :
        table           : Pandas DataFrame containing the Levenshtein ratios
        
    '''
    # Initiate parameters #
    company_list_1 = list(company_df_1['COMPANY_NEW'])
    company_list_2 = list(company_df_2['COMPANY_NEW'])
        
    # Make SBSU (rows) and cannabis (columns) companies into matrix of zeroes #
    rows = len(company_list_2) 
    cols = len(company_list_1) # SBSU_companies
    distance = np.zeros((rows, cols), dtype=float)
    
    # Compute the distance #
    for j in range(cols):
        for i in range(rows):
            ## Round the ratios to 3 decimal places ##
            distance[i][j] = round(lev.ratio(company_list_2[i], company_list_1[j]), 3)
    
    # Import the distances into Pandas DataFrame #
    table = pd.DataFrame(distance, index=company_list_2, columns=company_list_1)
    
    # Get top n ratios per table row #
    topFew = np.argsort(-table.values, axis=1)[:, :top_few]
    arr = []
    for i in range(len(topFew)):
        temp = []
        for j in range(top_few):
            if table.iloc[i, topFew[i][j]] < minimum_ratio :
                temp.append((np.nan, np.nan))
            else:
                temp.append((company_df_1.loc[company_df_1['COMPANY_NEW'] == table.columns.values[topFew[i][j]], 'Company'].iloc[0], 
                             company_df_1.loc[company_df_1['COMPANY_NEW'] == table.columns.values[topFew[i][j]], 'isin'].iloc[0],
                             table.iloc[i, topFew[i][j]]))
        arr.append(temp)
        
    # Import the ratios into Pandas DataFrame and manipulate the columns #
    top_ratios = pd.DataFrame(arr, index=table.index)
    name_columns = ['High_Sim_Name_' + str(i+1) for i in range(len(top_ratios.columns))]
    top_ratios.columns = name_columns
    for i in range(len(name_columns)):
        top_ratios['High_Sim_Ratio_%s' % str(i+1)] = top_ratios.loc[:, 'High_Sim_Name_%s' % str(i+1)].str[-1]
        top_ratios['High_Sim_ISIN_%s' % str(i+1)] = top_ratios.loc[:, 'High_Sim_Name_%s' % str(i+1)].str[1]
        top_ratios['High_Sim_Name_%s' % str(i+1)] = top_ratios.loc[:, 'High_Sim_Name_%s' % str(i+1)].str[0]
    # Rearrange the columns #
    columns = [column for i in range(len(name_columns)) \
               for column in list(top_ratios.columns) 
               if int(column.split('_')[-1]) == i+1]
    top_ratios = top_ratios[columns]
    
    return table, top_ratios

######################################################################################################################################

# Parse the GEM Global Coal-Fired Power Plants Database #
## String containing local download path ##
download_path = os.environ['USERPROFILE'] + r'\Dropbox\ESG Research\Energy Extractives\Global Energy Monitor'
## Headers ##
headers = {'User-Agent': 'Mozilla/5.0 (Macintosh; Intel Mac OS X 10_11_6)' + \
           'AppleWebKit/537.36 (KHTML, like Gecko) Chrome/61.0.3163.100 Safari/537.36'}
## Create variable that contains common company abbreviations ##
abb_patterns = r'\b(AND|&)(\sCOMPANY|\sCO)\b|\bA\.?/?B\.?\b|\bA\.?/?G\.?\b|\bA\.?\s?/?S\.?\b|\bANONIM SIRKETI\b|' + \
               r'\bAKTIEBOLAG\b|\bAKTIENGESELLSCHAFT\b|\bB\.?/?V\.?\b|\bBERHAD\b|\bBHD\.?\b|' + \
               r'\b(CO,?\.?|COMPANY)(,?\sINC\.?|,?\sINCORPORATED)\b|\b(CO,?\.?|COMPANY) INC(\./THE)?\b|' + \
               r'\b(CO,?\.?|COMPANY)(\sLTD\.?|\sLIMITED)?\b|\b(CORP\.?|CORPORATION)(\sLTD\.?|\sLIMITED)?\b|' + \
               r'\bGESELLSCHAFT MIT BESCHRÄNKTER HAFTUNG\b|\bGMBH(\s&\sCO\.?\s?KG)?\b|\bGROUP\b|\bHLDGS?\b|\bHOLDINGS?\b|' + \
               r'\bINC\.?\b|\bINCORPORATED\b|\bIND\.?\b|\bJOINT STOCK(\sCOMPANY|\sCO)\b|\bJ(\.\s?)?S(\.\s?)?C(\.\s?)?\b|' + \
               r'\bLIMITED LIABILITY(\sCOMPANY|\sCO)\b|\bLIMITED L(\.\s?)?C(\.\s?)?\b|\bL(\.\s?)?L(\.\s?)?C(\.\s?)?\b|' + \
               r'\bLIMITED\b|\bL(\.\s?)?P(\.\s?)?\b|\b(PTY\s)?LTD\.?\b|\bN\.?,?/?V\.?,?\b|' + \
               r'\bOSAKEYHTIÖ\b|\b[ØO]\.?/?Y\.?\b|\bOYJ\b|\bPERSEROAN TERBATAS\b|\bP\.?T\.?\b|' + \
               r'\bPUBLIC J(\.\s?)?S(\.\s?)?C(\.\s?)?\b|\bP(\.\s?)?J(\.\s?)?S(\.\s?)?C(\.\s?)?\b|' + \
               r'\bPUBLIC COMPANY LIMITED\b|\bPUBLIC JOINT STOCK (CO.?|COMPANY)\b|\bPUBLIC COMPANY LIMITED\b|' + \
               r'\bP(\.\s?)?C(\.\s?)?L(\.\s?)?\b|\bP(\.\s?)?L(\.\s?)?C(\.\s?)?\b|\bPUBLIC LIMITED(\sCO|\sCOMPANY)\b|' + \
               r'\bQ(\.\s?)?P(\.\s?)?S(\.\s?)?C(\.\s?)?\b|\bQ(\.\s?)?S(\.\s?)?C(\.\s?)?\b|' + \
               r'\bS\.?,?/?A\.?,?\b|\bS(\.\s?)?A(\.\s?)?E(\.\s?)?\b|\bS\.?,?/?E\.?,?\b|' + \
               r'\bSOCIETÀ PER AZIONI\b|\bSOCIETAS EUROPEAE\b|\bSOCIÉTÉ ANONYME\b|\bS(\.\s?)?P(\.\s?)?A(\.\s?)?\b|' + \
               r'\bT(\.\s?)?B(\.\s?)?K(\.\s?)?\b'
SBSU_data = pd.read_excel(download_path + '\\Universe.xlsx', sheet_name='Unv')
#SBSU_data.sort_values(by=['sciBetaId','lastDate'], ascending=[True,False], inplace=True)
SBSU_data.drop_duplicates(subset=['Company'], inplace=True)
SBSU_data = preprocess_names(SBSU_data, abb_patterns)
# urgewald_data = coal_entities 
# urgewald_data = preprocess_names(urgewald_data, abb_patterns)

# Run the Levenshtein similarity test and merge the datasets #
# table, top_ratios = get_closest_match(SBSU_data, urgewald_data, minimum_ratio=0.6, top_few=3)
# top_ratios.to_excel(download_path + '\\GEM_SIM_TEST.xlsx', sheet_name='RESULTS')

# # Preprocessing of Parent column #
# ## Change to upper case ##
# coal_plants['Parent'] = coal_plants['Parent'].str.upper()
# ## Remove / Replace characters ##
# coal_plants['Parent'] = coal_plants['Parent'].str.replace(r'\bTBD\b|\bUNKNOWN\b', '')
# coal_plants['Parent'] = coal_plants['Parent'].str.replace(r'(\d+)\W\%', r'\1%')
# coal_plants['Parent'] = coal_plants['Parent'].str.replace(r'(\d+),(\d+)\s?\%', r'\1.\2%')
# coal_plants['Parent'] = coal_plants['Parent'].str.replace(r'\(\d+\.?(\d+)?\% CITY OF \w+\)|\(MAIN OWNER\)', '')
# coal_plants['Parent'] = coal_plants['Parent'].str.replace(r'\s{2,}', ' ').str.strip()
# coal_plants['Parent'] = coal_plants['Parent'].replace('', np.nan)
# ## Create a new column Parent_New and Percent_Ownership to store the individual entities ##
# parents_cum = list(coal_plants['Parent'].dropna().drop_duplicates())
# parents_ind = [[parent_cum, re.sub(r'\(?\d+(\.\d+)?\%\)?', '', parent).strip(), re.search(r'\d+(\.\d+)?\%', parent).group(0)]
#                 if '%' in parent
#                 else [parent_cum, parent, np.nan]
#                 for parent_cum in parents_cum 
#                 for parent in parent_cum.split(', ')]
# parents_ind = pd.DataFrame(parents_ind, columns=['Parent', 'Company', 'Percent_Ownership'])
# coal_plants = pd.merge(coal_plants, parents_ind, how='left', on='Parent')

# # Use Levenshtein to assess the number of matched securities #
# ## Create variable that contains common company abbreviations ##
# abb_patterns = r'\b(AND|&)(\sCOMPANY|\sCO)\b|\bA\.?/?B\.?\b|\bA\.?/?G\.?\b|\bA\.?\s?/?S\.?\b|\bAKTIEBOLAG\b|\bAKTIENGESELLSCHAFT\b' + \
#                r'\bB\.?/?V\.?\b|\bBERHAD\b|\bBHD\.?\b|' + \
#                r'\b(CO,?\.?|COMPANY)(,?\sINC\.?|,?\sINCORPORATED)\b|\b(CO,?\.?|COMPANY) INC(\./THE)?\b|' + \
#                r'\b(CO,?\.?|COMPANY)(\sLTD\.?|\sLIMITED)?\b|\b(CORP\.?|CORPORATION)(\sLTD\.?|\sLIMITED)?\b|' + \
#                r'\bGESELLSCHAFT MIT BESCHRÄNKTER HAFTUNG\b|\bGMBH(\s&\sCO\.?\s?KG)?\b|\bGROUP\b|\bHLDGS?\b|\bHOLDINGS?\b|' + \
#                r'\bINC\.?\b|\bIND\.?\b|\bJOINT STOCK(\sCOMPANY|\sCO)\b|\bJ(\.\s?)?S(\.\s?)?C(\.\s?)?\b|' + \
#                r'\bLIMITED LIABILITY(\sCOMPANY|\sCO)\b|\bLIMITED L(\.\s?)?C(\.\s?)?\b|\bL(\.\s?)?L(\.\s?)?C(\.\s?)?\b|' + \
#                r'\bLIMITED\b|\b(PTY\s)?LTD\.?\b|\bN\.?,?/?V\.?,?\b|\bOSAKEYHTIÖ\b|\b[ØO]\.?/?Y\.?\b|\bOYJ\b|' + \
#                r'\bPERSEROAN TERBATAS\b|\bP\.?T\.?\b|' + \
#                r'\bPUBLIC J(\.\s?)?S(\.\s?)?C(\.\s?)?\b\bP(\.\s?)?J(\.\s?)?S(\.\s?)?C(\.\s?)?\b|\bPUBLIC COMPANY LIMITED\b|' + \
#                r'\bPUBLIC JOINT STOCK (CO.?|COMPANY)\b|\bPUBLIC COMPANY LIMITED\b|' + \
#                r'\bP(\.\s?)?C(\.\s?)?L(\.\s?)?\b|\bP(\.\s?)?L(\.\s?)?C(\.\s?)?\b|\bPUBLIC LIMITED(\sCO|\sCOMPANY)\b|' + \
#                r'\bS\.?,?/?A\.?,?\b|\bS(\.\s?)?A(\.\s?)?E(\.\s?)?\b|\bS\.?,?/?E\.?,?\b|' + \
#                r'\bSOCIETÀ PER AZIONI\b|\bSOCIETAS EUROPEAE\b|\bSOCIÉTÉ ANONYME\b|\bS(\.\s?)?P(\.\s?)?A(\.\s?)?\b|' + \
#                r'\bT(\.\s?)?B(\.\s?)?K(\.\s?)?\b'
# ## Import relevant datasets (SBSU and Urgewald) and preprocess the company names ##
# SBSU_data = pd.read_excel(download_path + '\\UniverseID.xlsx', sheet_name='Unv')
# SBSU_data.sort_values(by=['sciBetaId','lastDate'], ascending=[True,False], inplace=True)
# SBSU_data.drop_duplicates(subset=['Company'], inplace=True)
# SBSU_data = preprocess_names(SBSU_data, abb_patterns)
# coal_plants_abg = coal_plants[['Company']]
# coal_plants_abg['Company'] = coal_plants['Company'].str.replace(r'\bOTHER\b', '')
# coal_plants_abg['Company'] = coal_plants_abg['Company'].replace('', np.nan)
# coal_plants_abg.dropna(inplace=True)
# coal_plants_abg.drop_duplicates(subset=['Company'], inplace=True)
# coal_plants_abg = preprocess_names(coal_plants_abg, abb_patterns)      
# ## Run the Levenshtein similarity test and merge the datasets ##
# table, top_ratios = get_closest_match(SBSU_data, coal_plants_abg, minimum_ratio=0.5, top_few=3)
# top_ratios.to_excel(download_path + '\\GEM_SIM_TEST.xlsx', sheet_name='RESULTS')