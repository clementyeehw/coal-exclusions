# Import Python libraries #
from bs4 import BeautifulSoup
from concurrent.futures import ThreadPoolExecutor
from functools import partial
import datetime as dt
import gc
# import Levenshtein as lev
import numpy as np
import os
import pandas as pd
import re
import requests
from selenium import webdriver
from selenium.webdriver import DesiredCapabilities
from selenium.webdriver.common.by import By
from selenium.webdriver.firefox.options import Options
from selenium.webdriver.support.ui import WebDriverWait
from selenium.webdriver.support import expected_conditions as EC
import time


######################################################################################################################################
''' 
##########          ########## 
            LOGIN 
##########          ##########  
'''
    
def start_webpage(log_path, download_path=None, mime=None):
    ''' 
    Initiate a Firebox browser session.
    Parameters:
        log_path      : String containing the filepath of the geckodriver log file
        download_path : String containing the local download filepath.
                        Default value is None
        mime          : Mime file type(s) that would be downloaded.
                        Default value is None
    Return:
        browser       : Instantiated Firefox browser
    '''
    # Initiate a Firefox browser session #
    profile = webdriver.FirefoxProfile()

    profile.set_preference('general.useragent.override', 'Mozilla/5.0' + \
                           '(Macintosh; Intel Mac OS X 10_11_6)' + \
                           'AppleWebKit/537.36 (KHTML, like Gecko)' + \
                           ' Chrome/61.0.3163.100 Safari/537.36')
    profile.set_preference('devtools.jsonview.enabled', False)
    profile.set_preference('dom.webdriver.enabled', False)
    profile.set_preference('useAutomationExtension', False)
    profile.set_preference('accessibility.blockautorefresh', True)
    profile.set_preference('intl.accept_languages', 'en-US, en')
    profile.native_events_enabled = True
    profile.update_preferences()
    desired = DesiredCapabilities.FIREFOX
    
    if download_path != None:
        profile.set_preference('browser.download.folderList', 2)
        profile.set_preference('browser.download.dir', download_path)
        profile.set_preference('pdfjs.disabled', True)
        if mime != None:
            profile.set_preference('browser.helperApps.neverAsk.saveToDisk', mime)
        else:
            raise ValueError('Please provide MIME type in the argument!')
                       
    options = Options()
    # options.add_argument('--headless')
    browser = webdriver.Firefox(firefox_profile=profile,
                                desired_capabilities=desired,
                                options=options,
                                service_log_path=log_path)
    
    return browser


def login_webpage(browser):
    ''' 
    Login to FinanceMap (FM).
    Parameters:
        browser       : Blank session
    Return:
        browser       : Instantiated FM session
    '''
    # Initiate parameter #
    fm_url = 'https://financemap.org/evoke/user/1/login'
    
    # Send GET request to FM portal #
    browser.get(fm_url)
    time.sleep(2)
    
    # Login to FM portal #
    ## Input login credentials ##
    username = browser.find_element_by_id('user_login')
    username.send_keys('clement.yee@scientificbeta.com')
    password = browser.find_element_by_id('user_password')
    password.send_keys('Sjiboy88!')
    ## Submit login credential ##
    login_button = browser.find_element_by_class_name('btn-primary')
    login_button.click() 
    
    return browser

######################################################################################################################################
''' 
##########                  ########## 
            WEBSCRAPPING 
##########                  ##########  
'''

def chunks(lst, n):
    '''
    Generate successive n-sized chunks from list.
    Parameters:
        lst           : List containing the elements to be broken into chunks
        n             : Size of each chunk
    Return:
        chunked_list  : List containing n-sized chunks
    '''
    chunked_list = [lst[i:i + n] for i in range(0, len(lst), n)]
    
    return chunked_list


def get_listed_funds(browser):
    ''' 
    Retrieve the main landing page for all listed funds in FM.
    Parameter:
        browser    : Instantiated FM session
    Return:
        coal_mines : DataFrame containing local weblinks of company mines
    '''    
    # Initiate parameters #
    ## Root url ##
    root_url = 'https://financemap.org'
    ## Empty array ##
    funds = np.empty((0,2), str)
    
    # Obtain all countries #
    search_button = WebDriverWait(browser, 20).until(
        EC.element_to_be_clickable((By.CLASS_NAME, 'search-button')))
    soup = BeautifulSoup(browser.page_source, 'html.parser')
    countries = (soup.find('select', {'id':'select-country'})
                     .find_all('option', {'value':re.compile(r'^(?!CONTINENT)')}))
    countries = [country['value'] for country in countries]
    
    # Populate top 200 funds by AUM for each country #
    for country in countries:
        browser.find_element_by_xpath("//option[@value='%s']" % country).click()
        search_button.click()
        buffer = WebDriverWait(browser, 60).until(
                    EC.element_to_be_clickable((By.ID, 'entity-list')))
    
        ## Obtain all Asset Management Groups ##
        soup = BeautifulSoup(browser.page_source, 'html.parser')
        rows = soup.find('div', {'class':'table-body'}).find_all('div', {'class':'row'})
        top_funds = np.array([
                        [row.find('a', {'href':re.compile('/fund')}).text.strip(),
                         root_url + row.find('a', {'href':re.compile('/fund')})['href'],
                         root_url + row.find('a', 
                            {'href':re.compile('/evoke|/financialgroup|/assetmanager')})['href']] 
                            for row in rows
                            if row.find('a', {'href':re.compile('/fund')})])
        groups = [root_url + row.find('a', 
                    {'href':re.compile('/evoke|/financialgroup|/assetmanager')})['href']
                  for row in rows]
        
        ## Refresh each webpage ##
        browser.refresh()
        search_button = WebDriverWait(browser, 20).until(
            EC.element_to_be_clickable((By.CLASS_NAME, 'search-button')))
    ## Remove duplicates ##
    groups = list(set(groups))
    
    # Loop through each Asset Management Group #
    for group in groups:
        ## Send GET request #
        browser.get(group)
        time.sleep(2)
        
        ## Obtain listed funds under each Group ##
        soup = BeautifulSoup(browser.page_source, 'html.parser')
        try:
            rows = soup.find('div', {'class':'table-body'}).find_all('div', {'class':'row'})
        except Exception:
            fund = top_funds[top_funds[:, -1] == group][:, :-1]
            funds = np.vstack((funds, fund))
        else:
            fund = [[row.find('a', {'href':re.compile('/fund')}).text.strip(), 
                     root_url + row.find('a', {'href':re.compile('/fund')})['href']]
                    for row in rows]
            funds = np.vstack((funds, np.array(fund)))
    
    # Close browser #
    browser.close()
    
    # Convert into DataFrame #
    funds = pd.DataFrame(funds, columns=['FUND', 'WEBLINK'])
    funds.to_excel(download_path + '\\FUNDS.xlsx', sheet_name='FUNDS', index=False)
    
    return funds


def get_coal_companies(filepath):
    ''' 
    Retrieve the listed coal mining companies for all listed funds in FM.
    Parameter:
        filepath   : Filepath (including filename) of Excel file containing FM weblinks
    Return:
        companies  : DataFrame containing local weblinks of coal producers
    ''' 
    # Initiate parameters #
    ## Empty array ##
    producers = np.empty((0,4), str)
    ## Browsers ##
    browsers = [start_webpage(download_path + '\\geckodriver.log') for i in range(max_workers)]
    browsers = [login_webpage(browser) for browser in browsers]
    
    # Ingest DataFrame to retrieve local weblinks #
    funds = pd.read_excel(filepath)
    weblinks = list(funds['WEBLINK'])
    chunked_weblinks = chunks(weblinks, max_workers)
    
    # Loop through each target weblink and get coal mining companies # 
    with ThreadPoolExecutor() as executor:
        for chunked_weblink in chunked_weblinks:
            results = executor.map(get_coal_company, browsers, chunked_weblink)
            for result in results:
                if len(result) != 0:
                    producers = np.vstack((producers, result))
                    
    # Close browsers #
    [browser.close() for browser in browsers]       

    # Convert into DataFrame #
    producers = pd.DataFrame(producers, columns=['COMPANY', 'PRODUCTION (TONNES)', 'TICKER', 'WEBLINK'])
    producers.to_excel('COAL_PRODUCERS.xlsx', sheet_name = 'MINING', index=False)        
                    
    return producers


def get_coal_company(browser, weblink):
    '''
    Retrieve the listed coal mining companies for target FM fund.
    Parameters:
        browser    : Instantiated FM browser
        weblink    : Target local weblink
    Return:
        producers  : Numpy array containing the coal producer's details
    '''
    # Initiate parameter #
    producers = np.empty((0,4), str)
    
    try:
        # Send GET request for target weblink #
        browser.get(weblink)
    except Exception:
        # Print status message #
        print('Weblink is not valid: %s' % weblink)
    else:
        # Cater waiting time #
        time.sleep(2)
        # Apply Beautiful Soup to get all web contents #
        soup = BeautifulSoup(browser.page_source, 'html.parser')
        coal_flag = soup.find('div', {'id':'Panel-Coal_Coal'})
        if coal_flag:
            rows = coal_flag.find_all('div', 
                            {'class':re.compile('tdii-shareholding')})
            producer = [[row.find('div', {'class':'company-name'}).text.strip(),
                         (row.find('div', {'class':'port-comp-prod'})
                             .text.encode('ascii', 'xmlcharrefreplace').decode('utf-8')
                             .strip()),
                         row.find('div', {'class':'company-listing-ticker'}).text.strip(),
                         weblink]
                        for row in rows]
            producers = np.vstack((producers, np.array(producer)))
    
    # Convert into DataFrame #
    producers = pd.DataFrame(producers, columns=['COMPANY', 'PRODUCTION (TONNES)', 'TICKER', 'WEBLINK'])
    
    return producers


## Number of threads ##
max_workers = 10
## String containing local download path ##
download_path = os.environ['USERPROFILE'] + r'\Dropbox\ESG Research\Energy Extractives\FinanceMap'